// Create documents to use

db.fruits.insertMany([
	{
		name: "Apple",
		color: "Red",
		stock: 20,
		price: 40,
		supplierId: 1,
		onSale: true,
		origin: ["Philippines", "US"]
	},

	{

		name: "Banana",
		color: "Yellow",
		stock: 15,
		price: 20,
		supplierId: 2,
		onSale: true,
		origin: ["Philippines", "Ecuador"]
	},

	{

		name: "Kiwi",
		color: "Green",
		stock: 25,
		price: 50,
		supplierId: 1,
		onSale: true,
		origin: ["US", "China"]
	},

	{

		name: "Mango",
		color: "Yellow",
		stock: 10,
		price: 120,
		supplierId: 2,
		onSale: false,
		origin: ["Philippines", "India"]
	},

]);


// [SECTION] MongoDB Aggregation

/*
		Used to generate manipulated data and perform operations to create filtered results that help analyzing data.

*/

//  Using the aggregate method

/*
		The $match method is used to pass the documents that meet the specified conditions/s to the next pipeline stage/aggregation process

		Syntax:
		{$match: {field: value}}

		The group is used to group elements together and field-value pairs using the data from grouped elements
		Syntax:
		{$group: {_id: "value" fieldResult: "valueResult"}}

		- Using both "$match" and "$group" along with aggregation will find for products that are on sale and will group the total amount of stocks for all suppliers found.
	  - Syntax
  	  - db.collectionName.aggregate([
      { $match: { fieldA, valueA } },
      { $group: { _id: "$fieldB" }, { result: { operation } } }
   	 ])
 	 - The "$" symbol will refer to a field name that is available 	in the documents that are being aggregated on.
  	- The "$sum" operator will total the values of all "stock" fields in the returned documents that are found using the "$match" criteria.

*/


db.fruits.aggregate([
	{$match: {onSale: true} },
	{$group: {_id: "$supplierId", total: {$sum: "$stock"}}}
	]);

// Field projection with aggregation
db.fruits.aggregate([
	{$match: {onSale: true} },
	{$group: {_id: "$supplierId", total: {$sum: "$stock"}}},
	{$project: {_id: 0}}
	]);

// Sorting aggregated results

/*
	{$sort: {total: 1}}	

		 1 ascending order
		-1 descending order

*/

db.fruits.aggregate([
	{$match: {onSale: true} },
	{$group: {_id: "$supplierId", total: {$sum: "$stock"}}},
	{$sort: {total: 1} }
	]);

// Aggregating result based on array fields

/*
		deconstruct an array

*/

db.fruits.aggregate([
	{$unwind: "$origin"}
	]);

db.fruits.aggregate([
	{$unwind: "$origin"}
	{$group: {_id: "$origin", kinds: {$sum: 1}}}
	]);

// [SECTION] Other Aggregate Stages

//$count all yellow fruits

db.fruits.aggregate([
		{$match: {color: "Yellow"}},
		{$count: "Yellow Fruits"}
	]);

// $avg

db.fruits.aggregate([
		{$match: {color: "Yellow"}},
		{$group: {_id: "$color", yellowFruitsStock: {$avg: "$stock"}}}
	]);


// $min & $max

db.fruits.aggregate([
		{$match: {color: "Yellow"}},
		{$group: {_id: "$color", yellowFruitsStock: {$max: "$stock"}}}
	]);


db.fruits.aggregate([
		{$match: {color: "Yellow"}},
		{$group: {_id: "$color", yellowFruitsStock: {$min: "$stock"}}}
	]);